# LocalGov Drupal Example project - for Greenwich

You should be able to start this in Gitpod by clicking the following link and
logging into Gitpod with your Girlab account.

[Open in Gitpod](https://gitpod.io/#https://gitlab.com/opencode/localgov-drupal-example/)

## Git branching model:

Someone once said, it doesn't matter which git branching model your team uses,
as long as you choose one and use it.

I generally go with a modififed version of Git flow:

See:

 - https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow
 - https://datasift.github.io/gitflow/IntroducingGitFlow.html


